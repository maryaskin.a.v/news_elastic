from django.db import models


class BaseSeoAbstract(models.Model):
    """ SEO abstract class. """
    created = models.DateTimeField('Дата добавления', auto_now_add=True)
    updated = models.DateTimeField('Дата обновления', auto_now=True)

    seo_title = models.CharField(max_length=255, blank=True, null=True)
    seo_description = models.TextField(blank=True, null=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self._meta.verbose_name
