from django.contrib import admin

# Register your models here.
from news.models import Rubric, NewsAuthor, News


@admin.register(Rubric)
class RubricAdmin(admin.ModelAdmin):
    pass


@admin.register(NewsAuthor)
class NewsAuthorAdmin(admin.ModelAdmin):
    pass


@admin.register(News)
class NewsAdmin(admin.ModelAdmin):
    pass
