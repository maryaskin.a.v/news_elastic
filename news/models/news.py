import html
import os
from zipfile import ZipFile

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.utils.functional import cached_property
from django.utils.html import strip_tags
from django.views.generic.dates import timezone_today

from utils.models import BaseSeoAbstract


class Rubric(BaseSeoAbstract):
    name = models.CharField("Название", unique=True, max_length=20)
    slug = models.SlugField("Слаг", unique=True)
    show_in_filter = models.BooleanField("Показывать в трее", default=False)
    ordered = models.PositiveIntegerField("Порядок", default=0)

    class Meta:
        verbose_name = "Рубрика"
        verbose_name_plural = "Рубрики"
        ordering = ("ordered",)

    def __str__(self):
        return self.name


class NewsAuthor(models.Model):
    name = models.CharField("Автор", max_length=150)
    image = models.ImageField("Изображение автора", upload_to="news/author/", blank=True, null=True)
    regalia = models.CharField("Регалии автора", max_length=250, blank=True, null=True)

    class Meta:
        verbose_name = "Автор новости"
        verbose_name_plural = "Авторы новостей"

    def __str__(self):
        return self.name


class News(BaseSeoAbstract):
    """News model for public new company news."""

    USUAL = 1
    CASE_NEW = 3
    BOOK = 4
    ARTICLE = 5
    INTERVIEW = 6
    EVENT = 7
    TEST = 8
    CARD = 9
    CASE_OLD = 10

    NEWS_TYPE_CHOICES = (
        (USUAL, "Обычная"),
        (CASE_NEW, "Кейс"),
        (BOOK, "Книга"),
        (ARTICLE, "Статья"),
        (INTERVIEW, "Интервью"),
        (EVENT, "Мероприятие"),
        (TEST, "Тест"),
        (CARD, "Карточка"),
        (CASE_OLD, "Старый кейс"),
    )

    view_in_mobile_app = models.BooleanField("Показывать в мобильном приложении", default=True, db_index=True)
    publish = models.BooleanField("Опубликованно", default=True, db_index=True)
    rubric = models.ForeignKey(Rubric, verbose_name="Рубрика", on_delete=models.PROTECT, related_name="news")
    title = models.CharField("Заголовок", max_length=60)
    subtitle = models.CharField("Подзаголовок", max_length=100)
    title_photo = models.ImageField("Заглавная фотография", upload_to="news/title/")
    title_photo_mobile = models.ImageField(
        "Заглавная фотография (мобильная)", upload_to="news/title_mobile/", null=True, blank=True
    )
    social_photo = models.ImageField("Фотография для соц. сетей", upload_to="news/social/", null=True)
    date = models.DateField("Дата", default=timezone_today)
    reading = models.PositiveIntegerField("Время на прочтение (мин.)")
    is_best = models.BooleanField("В лучшее", default=False)

    author = models.ForeignKey(NewsAuthor, verbose_name="Автор", on_delete=models.PROTECT, null=True, blank=True)

    news_type = models.PositiveSmallIntegerField("Тип новости", choices=NEWS_TYPE_CHOICES, default=USUAL)

    description = models.TextField("Опиисание", blank=True, null=True)
    partner_name = models.CharField("Название партнера", max_length=150, blank=True, null=True)
    partner_image = models.ImageField("Изображение партнера", upload_to="news/partner/", blank=True, null=True)
    partner_link = models.URLField("Ссылка на партнера", blank=True, null=True)
    cover = models.ImageField("Обложка для книги", upload_to="news/cover/", blank=True, null=True)
    is_notification_bot = models.BooleanField("Отправлено уведомления", default=False)
    full_text = models.TextField("Полный текст", blank=True, null=True)
    is_private = models.BooleanField("Приватная статья", default=False)

    class Meta:
        verbose_name = "Новость"
        verbose_name_plural = "Новости"
        ordering = ("-date", "-created")

    def __str__(self):
        return self.title

    def clean(self):
        if self.news_type == News.BOOK:
            if not self.cover:
                raise ValidationError({"cover": "Поле не может быть пустым."})
        else:
            self.cover.delete(False)

    def get_full_text(self):
        def _get_str_or_empty(obj, attr):
            if getattr(obj, attr):
                return f"{getattr(obj, attr)} "
            return ""

        text = _get_str_or_empty(self, "title")
        text += _get_str_or_empty(self, "subtitle")
        text += _get_str_or_empty(self, "description")
        for block in self.blocks.all():
            if block.block_type == 1:  # Текст
                text += _get_str_or_empty(block, "text")
            elif block.block_type == 2:  # Цитата
                text += _get_str_or_empty(block, "text")
            elif block.block_type == 3:  # Видео
                text += _get_str_or_empty(block, "title")
                text += _get_str_or_empty(block, "text")
            elif block.block_type == 4:  # Аудио
                text += _get_str_or_empty(block, "title")
                text += _get_str_or_empty(block, "text")
            elif block.block_type == 5:  # Карусель изображений
                for image in block.carousel.images.all():
                    text += _get_str_or_empty(image, "sign")
            elif block.block_type == 6:  # Книга
                pass
            elif block.block_type == 7:  # Легенда
                pass
            elif block.block_type == 8:  # Мнение
                text += _get_str_or_empty(block, "title")
                text += _get_str_or_empty(block, "text")
            elif block.block_type == 9:  # Описание мероприятия
                pass
            elif block.block_type == 10:  # Заголовок
                text += _get_str_or_empty(block, "text")
            elif block.block_type == 11:  # Список
                for elem in block.elem_list.elems.all():
                    text += _get_str_or_empty(elem, "text")
            elif block.block_type == 12:  # Промо-блок
                text += _get_str_or_empty(block, "title")
            elif block.block_type == 13:  # Карточка
                text += _get_str_or_empty(block, "title")
                text += _get_str_or_empty(block, "text")
                text += _get_str_or_empty(block, "support_text")
                text += _get_str_or_empty(block, "address")
            elif block.block_type == 14:  # Зарезервирован за Тестом для мобилок
                pass
            elif block.block_type == 15:  # Опрос
                pass
            elif block.block_type == 16:  # Список кейса
                text += _get_str_or_empty(block.elem_case_list, "title")
                for elem in block.elem_case_list.elems.all():
                    text += _get_str_or_empty(elem, "text")
            elif block.block_type == 17:  # Баннер
                pass
        return html.unescape(strip_tags(text))

    @cached_property
    def archive_filepath(self):
        keep_characters = (" ", ",", ".", "_", "-")
        filename = "".join(c for c in f"{self.title}.zip" if c.isalnum() or c in keep_characters)
        filepath = os.path.join(settings.NEWS_ARCHIVE_FOLDER, filename)
        return filepath

    def get_materials(self):
        materials_count = self.materials.count()
        if materials_count == 1:
            return self.materials.first().file.path
        elif materials_count > 1:
            if os.path.exists(self.archive_filepath):
                return self.archive_filepath
            return self.create_archive()

    def create_archive(self):
        if self.materials.exists():
            with ZipFile(self.archive_filepath, "w") as my_zip:
                for material in self.materials.all():
                    my_zip.write(material.file.path, material.file.name.split("/")[-1])
            return self.archive_filepath

    @property
    def author_indexing(self):
        """Method for indexing field in elasticsearch"""
        if self.author is not None:
            return self.author.name
