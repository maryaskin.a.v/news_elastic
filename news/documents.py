from django.conf import settings
from django_elasticsearch_dsl import Document, fields
from django_elasticsearch_dsl.registries import registry
from elasticsearch_dsl import analyzer
from elasticsearch_dsl.connections import connections

from news.models import News

connections.create_connection(hosts=[x for x in settings.ELASTICSEARCH_DSL["default"]["hosts"]])

html_strip = analyzer(
    "html_strip",
    tokenizer="standard",
    filter=["standard", "lowercase", "stop", "snowball"],
    char_filter=["html_strip"],
)


@registry.register_document
class NewsDocument(Document):
    id = fields.IntegerField(attr="id")
    rubric = fields.ObjectField(
        properties={"id": fields.IntegerField(), "slug": fields.TextField(analyzer="keyword")},
    )
    title = fields.TextField(
        analyzer=html_strip,
        fields={
            "raw": fields.TextField(),
        },
    )
    description = fields.TextField(
        analyzer=html_strip,
        fields={
            "raw": fields.TextField(),
        },
    )
    is_private = fields.BooleanField()
    date = fields.DateField()

    class Index:
        name = "news"
        settings = {"number_of_shards": 1, "number_of_replicas": 0}

    class Django:
        model = News
